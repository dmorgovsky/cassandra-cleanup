package com.inneractive.chiron.cleanup

import java.io.File

import com.typesafe.config.ConfigFactory._
import com.typesafe.config.{Config, ConfigFactory}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.language.postfixOps

/**
  * Created by dmitry on 9/11/17.
  */
trait CleanupOptionsParser {

  def withOptionsFrom(args: Array[String])(f: CleanupOptions => Unit): Unit = {
    new scopt.OptionParser[CleanupOptions]("Cassandra cleanup") {
      head("Cassandra cleanup")
      opt[String]('c', "config").valueName("<path[,path]>").text("Coma separated pathes to Config Files").required.unbounded.action {
        (x, c) =>
          val config = buildConfig(x)
          //        parseGranularities(config) //TODO Dmitry: UGLY -- side effect inside
          c.copy(config = config)
      }
      opt[String]('k', "keyspace").valueName("<keyspace>").text("Keyspace").required
        .action { (x, c) => c.copy(keyspace = x)}
      opt[String]('t', "table").valueName("<table>").text("Table to clean").required
        .action { (x, c) => c.copy(table = x)}
      opt[String]('g', "granularity").valueName("<hourly|daily>").text("Granularity name").required
        .validate {
          case "hourly" | "daily" => success
          case _ => failure("'granularity' must be 'hourly' or 'daily'")
        }
        .action { (x, c) =>
          c.copy(timeUnit = x match {
            case "hourly" => 3600
            case "daily"  => 86400
          })
        }
      opt[String]('d', "date").valueName("<yyyy-MM|yyyy>").text("Delete up to timeframe").required.action {
        (x, c) =>
          val argPattern = c.timeUnit match {
            case 3600 => "yyyy-MM"
            case 86400 => "yyyy"
          }
          c.copy(timeFrameMillis = DateTime.parse(x, DateTimeFormat.forPattern(argPattern)).getMillis)
      }
      opt[Int]('f', "fetch").valueName("<fetchSize>").text("Number of rows to fetch")
        .action { (x, c) => c.copy(fetchSize = x)}
    }.parse(args, CleanupOptions())
      .foreach { cassandraCleanupOptions =>
        f(cassandraCleanupOptions)
      }
  }

  private[this] def buildConfig(pathes: String): Config =
    pathes split "," map { f => parseFile(new File(f trim)) } reduce { _ withFallback _ } resolve

}
case class CleanupOptions(config: Config = ConfigFactory.empty,
                          keyspace: String = null,
                          table: String = null,
                          timeUnit: Int = 0,
                          timeFrameMillis: Long = 0,
                          fetchSize: Int = 100000)

object DateFormatter {
  def formatted(dateTime: DateTime) = dateTime toString "yyyy-MM-dd 00:00:00+0000"
}

