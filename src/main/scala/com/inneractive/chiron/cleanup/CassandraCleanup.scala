package com.inneractive.chiron.cleanup

import java.lang.System.currentTimeMillis

import com.datastax.driver.core.SimpleStatement
import com.inneractive.chiron.cleanup.CassandraTools.{getPartitionKeys, withCassandraSession}

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.language.postfixOps

/**
  * Created by dmitry on 9/5/17.
  */
object CassandraCleanup extends App with CleanupOptionsParser {

  withOptionsFrom(args) { options =>
    // TODO take from config
    val cassandraConfig = options.config.getConfig("cassandra")
    withCassandraSession(
      cassandraConfig.getString("host") split ",",
      cassandraConfig.getString("username"),
      cassandraConfig.getString("password"),
      options.keyspace) { session =>

      val keyColumns = getPartitionKeys(session, options.keyspace, options.table)

      val selectStatement =
        new SimpleStatement(s"select ${keyColumns mkString ","} from ${options.keyspace}.${options.table}")
          .setFetchSize(options.fetchSize)
          .setReadTimeoutMillis(options.fetchSize) // timeout is proportional to fetchSize

      println(s"Fetching by query [$selectStatement], fetchSize = [${options.fetchSize}]...")

      val resultSet = session.execute(selectStatement)
      val columnDefinitions = resultSet.getColumnDefinitions
      val keyColumnsIndexes = keyColumns map { columnDefinitions.getIndexOf }

      val uniqueKeyValues = mutable.Set[Seq[AnyRef]]()
      val start = currentTimeMillis
      var count: Int = 0
      for (row <- resultSet) {
        if (row.getInt("time_unit") == options.timeUnit &&
          row.getTimestamp("time_frame").getTime <= options.timeFrameMillis) {
          uniqueKeyValues add (keyColumnsIndexes map { row.getObject })
        }

        count = count + 1
        if (count % (options.fetchSize * 100) == 0) println(s"+ ${count / 1000000}M") else
        if (count % (options.fetchSize * 10)  == 0) print("+") else
        if (count %  options.fetchSize        == 0) print("-")
      }
      val time = (currentTimeMillis - start) / 1000

      println()
      println(s"Found ${uniqueKeyValues.size} unique keys, it took [$time] seconds.")

      if (uniqueKeyValues nonEmpty) {
        val deleteStatement = s"delete from ${options.keyspace}.${options.table} where ${keyColumns.map(_ + "=?") mkString " and "}"
        println(s"Going to delete by query [$deleteStatement]...")

        val prepared = session.prepare(deleteStatement)
        count = 0
        uniqueKeyValues foreach { keyValues =>
          session.execute(prepared bind (keyValues: _*))
          count = count + 1
          if (count %  100 == 0) println(s"+ $count") else
          if (count %   10 == 0) print("+") else print("-")
        }
      }

      println()
      println("That's all Folks!")
    }
  }
}
