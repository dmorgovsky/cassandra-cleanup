package com.inneractive.chiron.cleanup

import scala.language.implicitConversions

/**
  * Created by dmitry on 7/4/17.
  */
// see https://stackoverflow.com/questions/25634455/simple-scala-pattern-for-using-try-with-resources-automatic-resource-manageme
class AutoCloseableWrapper[A <: AutoCloseable](protected val c: A) {
  def map[B](f: (A) => B): B = {
    try {
      f(c)
    } finally {
      c.close()
    }
  }

  def foreach(f: (A) => Unit): Unit = map(f)

  // Not a proper flatMap.
  def flatMap[B](f: (A) => B): B = map(f)

  // Hack :)
  def withFilter(f: (A) => Boolean) = this
}

object AutoCloseableWrapper {
  implicit def toAutoCloseableWrapper[A <: AutoCloseable](c: A): AutoCloseableWrapper[A] = new AutoCloseableWrapper(c)
}

/*

class DemoCloseable(val s: String) extends AutoCloseable {
  var closed = false
  println(s"DemoCloseable create $s")

  override def close(): Unit = {
    println(s"DemoCloseable close $s previously closed=$closed")
    closed = true
  }
}

object DemoCloseable {
  def unapply(dc: DemoCloseable): Option[(String)] = Some(dc.s)
}

object Demo extends App {
  import AutoCloseableWrapper._

  for (v <- new DemoCloseable("abc")) {
    println(s"Using closeable ${v.s}")
  }

  for (a <- new DemoCloseable("a123");
       b <- new DemoCloseable("b123");
       c <- new DemoCloseable("c123")) {
    println(s"Using multiple resources for comprehension. a.s=${a.s}. b.s=${b.s}. c.s=${c.s}")
  }

  val yieldInt = for (v <- new DemoCloseable("abc")) yield 123
  println(s"yieldInt = $yieldInt")

  val yieldString = for (DemoCloseable(s) <- new DemoCloseable("abc"); c <- s) yield c
  println(s"yieldString = $yieldString")

  println("done")
}

*/