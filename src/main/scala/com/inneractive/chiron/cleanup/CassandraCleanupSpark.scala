package com.inneractive.chiron.cleanup

import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.JavaConversions._

/**
  * Created by dmitry on 9/11/17.
  */
object CassandraCleanupSpark extends App with CleanupOptionsParser {
  withOptionsFrom(args) { options =>
    val sparkConf = options.config.getConfig("spark").entrySet.foldLeft(new SparkConf) { case (conf, entry) =>
      conf.set("spark." + entry.getKey, entry.getValue.unwrapped.toString)
    }
      .set("spark.cassandra.connection.host", "172.30.20.157,172.30.20.155,172.30.20.156") // TODO take from config
      .set("spark.cassandra.auth.username", "cassandra")                                   // TODO take from config
      .set("spark.cassandra.auth.password", "cassandra")                                   // TODO take from config

    val sparkContext = SparkContext.getOrCreate(sparkConf)

    CassandraConnector(sparkContext.getConf).withSessionDo { session =>
      val keys = CassandraTools.getPartitionKeys(session, options.keyspace, options.table)
      sparkContext
        .cassandraTable(options.keyspace, options.table)
        .select(keys map (ColumnName(_)) : _*)
        .distinct
        .filter(row => row.getInt("time_unit") == options.timeUnit && row.getDateTime("time_frame").getMillis <= options.timeFrameMillis)
//        .where("time_unit=? and time_frame='?' and date_time>='?' and date_time<='?'", options.timeUnit, options.beginDate, options.beginDate, options.endDate)
        .deleteFromCassandra(options.keyspace, options.table)
    }
  }
}
