package com.inneractive.chiron.cleanup

import com.datastax.driver.core.{Cluster, Row, Session}
import com.inneractive.chiron.cleanup.AutoCloseableWrapper._

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.language.postfixOps

/**
  * Created by dmitry on 9/11/17.
  */
object CassandraTools {

  def withCassandraSession(contactPoints: Seq[String], username: String, password: String, keyspace: String)(callback: (Session) => Unit) =
    for (cluster <- Cluster.builder.addContactPoints(contactPoints: _*).withCredentials(username, password).build;
         session <- cluster.connect(keyspace)) {
      callback(session)
    }

  def foreachRow(session: Session, statement: String)
                (callback: (Row) => Unit) = {
    val resultSet = session.execute(statement)
    for (row <- resultSet) {
      callback(row)
    }
  }

  def foreachTableWithKeys(session: Session, keyspace: String)
                          (callback: ((String, Seq[String])) => Unit) = {
    val tablesAndKeys = mutable.Map[String, (Seq[(String, Int)], Seq[(String, Int)])]().withDefaultValue((mutable.Seq[(String, Int)](), mutable.Seq[(String, Int)]()))
    foreachRow(session, s"select * from system_schema.columns") { row =>
      if (row.getString("keyspace_name") == keyspace) {
        val tableName = row.getString("table_name")
        val columnAndPosition = (row.getString("column_name"), row.getInt("position"))

        val (partitionKeys, clusteringKeys) = tablesAndKeys(tableName)
        tablesAndKeys(tableName) = row.getString("kind") match {
          case "partition_key" => (columnAndPosition +: partitionKeys, clusteringKeys)
          case "clustering" => (partitionKeys, columnAndPosition +: clusteringKeys)
          case _ => (partitionKeys, clusteringKeys)
        }
      }
    }

    tablesAndKeys
      .mapValues { case (partitionKeys, clusteringKeys) =>
        sortedByPosition(partitionKeys) ++ sortedByPosition(clusteringKeys).takeWhile(_ != "date_time")
//          .filter(key => key != "time_unit" && key != "time_frame")
      }
      .foreach { callback(_) }
  }

  def foreachTableWithPartitionKeys(session: Session, keyspace: String)
                                   (callback: ((String, Seq[String])) => Unit) = {
    val tablesAndKeys = mutable.Map[String, Seq[(String, Int)]]().withDefaultValue(mutable.Seq[(String, Int)]())
    foreachRow(session, s"select * from system_schema.columns") { row =>
      if (row.getString("keyspace_name") == keyspace) {
        val tableName = row.getString("table_name")
        val columnAndPosition = (row.getString("column_name"), row.getInt("position"))

        val partitionKeys = tablesAndKeys(tableName)
        tablesAndKeys(tableName) =
          if (row.getString("kind") == "partition_key") columnAndPosition +: partitionKeys else partitionKeys
      }
    }

    tablesAndKeys
      .mapValues { sortedByPosition }
      .foreach { callback(_) }
  }

  def getPartitionKeys(session: Session, keyspace: String, table: String) = {
    val keys = mutable.Set[(String, Int)]()
    foreachRow(session, s"select * from system_schema.columns") { row =>
      if (row.getString("keyspace_name") == keyspace && row.getString("table_name") == table) {
        if (row.getString("kind") == "partition_key")
          keys add (row.getString("column_name"), row.getInt("position"))
      }
    }
    sortedByPosition(keys toSeq)
  }

  private[this] def sortedByPosition(keys: Seq[(String, Int)]) = keys.sortBy(_._2).map(_._1)
}
